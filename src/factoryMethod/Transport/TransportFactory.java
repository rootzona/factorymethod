package factoryMethod.Transport;

public class TransportFactory {
    // klasa o nazwie TransportFactory
    private static final String TRUCK = "truck";
    private static final String PLANE = "plane";
    private static final String SHIP = "ship";
    private static final String BIKE = "bike";
    private static final String WALK = "walk";
    private static final String TANK = "tank";

    public static CargoTransport getTransport(String transportName){
        // implementacja logiki
        CargoTransport cargoTransport = null;
        // typ o nullowaj wartosci
        // warunki
        if(TRUCK.equalsIgnoreCase(transportName)) {
            cargoTransport = new Truck();
        } else if (PLANE.equalsIgnoreCase(transportName)) {
            cargoTransport = new Plane();
        } else if (SHIP.equalsIgnoreCase(transportName)) {
            cargoTransport = new Ship();
        } else if (BIKE.equalsIgnoreCase(transportName)) {
            cargoTransport = new Bike();
        } else if (WALK.equalsIgnoreCase(transportName)) {
            cargoTransport = new Walk();
        } else if (TANK.equalsIgnoreCase(transportName)) {
            cargoTransport = new Tank();
        }
        // zwracam typ o nazwie cargoTransport
        return cargoTransport;
    }
}
