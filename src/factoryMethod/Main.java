package factoryMethod;

import factoryMethod.Transport.CargoTransport;
import factoryMethod.Transport.TransportFactory;

public class Main {

    public static void main(String[] args) {
        // CargoTransport to interfejs, ktory pobiera z TransportFactory, gdzie mamy logike dzialania
        CargoTransport cargoTransport = TransportFactory.getTransport("Tank");
        //
        // cargoTransport to typ
        // metoda getTransport na klasie TransportFactory wywoluje, pobiera a nie tworzy
        // Truck to nazwa
        cargoTransport.process();
        // co dostaja klient? cargoTransport zwraca obiekt z logiki
        // (public static CargoTransport getTransport(String transportName))
    }
}
